<?php 
 $arrMenu = [
    "dashboard" => [
        'href' => 'index.html',
        'icon' => 'fa fa-home',
        'label' => 'Dashboard',
        'active' => true,
        'submenu' => [],
    ],
    "cadastro" => [
        'href' => 'javascript:;',
        'icon' => 'fa fa-file-text',
        'label' => 'Cadastro',
        'active' => false,
        'submenu' => [
            'cliente' => [
                'href' => '#',
                'label' => 'Cliente'
            ],   
            'fornecedor' => [
                'href' => '#',
                'label' => 'Fornecedor'
            ],   
            'usuario' => [
                'href' => '#',
                'label' => 'Usuário'
            ],    
            'produto' => [
                'href' => '#',
                'label' => 'Produtos'
            ], 
            'perfilAcesso' => [
                'href' => '#',
                'label' => 'Perfil de Acesso'
            ],   
        ],
    ],
    "relatorio" => [
        'href' => 'javascript:;',
        'icon' => 'fa fa-bar-chart-o',
        'label' => 'Relatorio',
        'active' => false,
        'submenu' => [
            'cliente' => [
                'href' => '#',
                'label' => 'Cliente'
            ],
            'faturamento' => [
                'href' => '#',
                'label' => 'Faturamento'
            ],
            'produtos' => [
                'href' => '#',
                'label' => 'Produtos'
            ],
        ],
    ],

 ];
ksort($arrMenu);
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <form class="sidebar-search" action="extra_search.html" method="POST">
                    <div class="form-container">
                        <div class="input-box">
                            <a href="javascript:;" class="remove"></a>
                            <input type="text" placeholder="Search..."/>
                            <input type="button" class="submit" value=" "/>
                        </div>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <?php foreach($arrMenu as $menu) { ?>
                <? $hasSubMenu = !empty($menu['submenu']); ?>
                <li class="<?php echo $menu['active'] ? 'start active' : '';?> ">
                <a href="<?php echo $menu['href'] ?>">
                <i class="<?php echo $menu['icon'] ?>"></i>
                <span class="title">
                <?php echo $menu['label'] ?>
                </span>
                <?php if ($hasSubMenu) { ?>
                    <span class="arrow "></span>
                <?php } else {?>
                    <span class="selected"></span>
                <?php } ?>
                </a>
                <?php if ($hasSubMenu) { 
                    ksort($menu['submenu']);    
                ?>
                    <ul class="sub-menu">
                        <?php foreach($menu['submenu'] as $subMenu) { ?>
                            <li>
                                <a href="<?php echo $menu['href'] ?>"><?php echo $subMenu['label'] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
            <?php } ?>           
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>