$('.more').click(function() {
    let parent = $(this).parent()[0];
    let color = parent.attributes.class.value.split(/\s+/)[1];
    let request = this.attributes.class.value.split(/\s+/)[1];
    $('.tabela').removeClass('grey blue green purple');
    $('.tabela').addClass(color);

    $.ajax({
        type: "get",
        data: {
          request:request
        },
        url: "DataRequestHandler.php",
        dataType: "json",
        async: false,
        success: function(data) {           
          result=data;
        }
    });  
      
    var table = $('.tabela-simples'); 
    let headers = Object.keys(result[0]);
    headers.forEach(function(item, index){ 
        headers[index] = item.charAt(0).toUpperCase() + item.slice(1)
    });
    table.find('tr').remove();    
    table.append('<tr><th>#</th><th>'+headers.join('</th><th>')+'</th></tr>');
    

    result.forEach(function(item, index) {       
        table.append('<tr><td>'+(index+1)+'</td><td>'+Object.values(item).join('</td><td>')+'</td>');
    });
});
