<?php
require_once 'DataRequest.php';

if (isset($_GET['request'])) {
     $dataRequest = new DataRequest();
     $result = '';
     switch ($_GET['request']){
        case 'cliente':
            $result = $dataRequest->dadosClientes();
            break;
        case 'usuario':
            $result = $dataRequest->dadosUsuarios();
            break;
        case 'fornecedor':
            $result = $dataRequest->dadosFornecedores();
            break;
     }
     echo json_encode($result);
}
?>